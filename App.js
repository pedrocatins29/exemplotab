import React, {Component} from 'react';
import TabNavigator from './src/routes';

export default class App extends Component {
  render() {
    return <TabNavigator />;
  }
}
