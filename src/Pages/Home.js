import React from 'react';
import {View, Text, Button} from 'react-native';

const Home = ({navigation}) => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignContent: 'center'}}>
      <Text> Tela Home </Text>
      <Button title="abre menu" onPress={() => navigation.openDrawer()} />
    </View>
  );
};

export default Home;
