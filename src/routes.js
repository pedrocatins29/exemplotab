import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import Home from './Pages/Home';
import Login from './Pages/Login';
import Signout from './Pages/Signout';

const TabNavigator = createDrawerNavigator(
  {
    Home: createBottomTabNavigator({
      Home: Home,
      Pesquisar: Login,
    }),
    Signout: Signout,
  },
  {
    drawerBackgroundColor: 'rgba(255,255,255,.9)',
    contentOptions: {
      activeTintColor: '#FFF',
      activeBackgroundColor: '#6b6e70',
    },
    drawerPosition: 'right',
  },
);

export default createAppContainer(TabNavigator);
